# Node JS Rest Api Boilerplate
### Domain Driven Design Approach 

## Tech
- nodejs 
- typescript
- firebase
- elasticsearch
- Queue (no tech decided yet)

## Development Requirements
- Docker (recommended)


## Setup step
- npm install
- npm run start:dev (run docker development with elasticsearch)

## Configuration location
- 2 way to add configuration
  * .env
  * docker-compose.dev.yml

#### Firebase should handle user authentication and authorization and other real time data (notification), elastic act as cache and a search engine will also use as graph database in the future. 
- why not Redis or RediSearch? Upon writing this readme. i wast able to find a good support for rediSearch for nodejs. but rediSearch is i think better than elastic. will decide soon which one to use.  

- For Queue, my eyes is on Bull `https://github.com/OptimalBits/bull` but this is a redis still not decided yet. will decide after completing other prerequisite 
 
