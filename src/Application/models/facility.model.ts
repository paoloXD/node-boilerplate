import { AddressModel } from './address.model'

interface PhiSettings {
  accreditationNumber: string
  key: string
  accreditationName: string
}

interface Settings {
  phic: PhiSettings
}

export interface FacilityModel {
  id: string
  name: string
  code: string
  address: AddressModel
  settings: Settings
}
