import { Mapper } from '@/Core/infra/Mapper'
import { Address } from '../domain/address'
import { AddressDTO } from '../dto/address.dto'

export class AddressMapper implements Mapper {
  public static toDto(addr: Address): AddressDTO {
    return {
      brgy: addr.brgy,
      lat: addr.lat,
      long: addr.long,
      province: addr.province,
      region: addr.region,
      zip: addr.zip
    }
  }
}
