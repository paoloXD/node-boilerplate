import { Result } from '@/Core/domain/Result'
import { ValueObject } from '@/Core/domain/ValueObject'

export interface AddressProps {
  brgy: string
  lat: number
  long: number
  province: string
  region: string
  zip: string
}

export class Address extends ValueObject<AddressProps> {
  private constructor(props: AddressProps) {
    super(props)
  }

  public static create(props: AddressProps): Result<Address> {
    return Result.ok<Address>(new Address(props))
  }

  get brgy(): string {
    return this.props.brgy
  }

  get lat(): number {
    return this.props.lat
  }

  get long(): number {
    return this.props.long
  }

  get province(): string {
    return this.props.province
  }

  get region(): string {
    return this.props.region
  }

  get zip(): string {
    return this.props.zip
  }
}
