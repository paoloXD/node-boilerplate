export interface AddressDTO {
  brgy: string
  lat: number
  long: number
  province: string
  region: string
  zip: string
}
