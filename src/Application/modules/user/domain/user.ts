import { AggregateRoot } from '@/Core/domain/AggregateRoot'

interface UserProps {
  email: string
  phoneNumber: string
  photoUrl: string
  firstName: string
  lastName: string
  middleName: string
  emailVerified: boolean
}

export class User extends AggregateRoot<UserProps> {}
