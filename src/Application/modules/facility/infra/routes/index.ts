import { Router } from 'express'
import { createFacilityController } from '../../useCases/create'

const facilityRoute = Router()

facilityRoute.post('/', (req, res) =>
  createFacilityController.execute(req, res)
)

export { facilityRoute }
