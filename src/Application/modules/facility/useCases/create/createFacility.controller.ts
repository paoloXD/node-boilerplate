import { BaseController } from '@/Core/infra/BaseController'
import { FacilityMapper } from '../../mappers/facility.map'
import { CreateFacilityDTO } from './createFacility.dto'
import { CreateFacilityUseCase } from './createFacility.useCase'

export class CreateFacilityController extends BaseController {
  private useCase: CreateFacilityUseCase

  constructor(useCase: CreateFacilityUseCase) {
    super()
    this.useCase = useCase
  }

  async executeImpl(): Promise<any> {
    const dto: CreateFacilityDTO = this.req.body as CreateFacilityDTO

    try {
      const facility = await this.useCase.execute(dto)
      return this.res.json(FacilityMapper.toDto(facility))
    } catch (error) {
      return this.res.status(500)
    }
  }
}
