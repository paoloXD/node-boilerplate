import { Address } from '@/Application/modules/shared/address/domain/address'
import { UseCase } from '@/Core/domain/UseCase'
import { Facility } from '../../domain/facility'
import { FacilityCode } from '../../domain/facilityCode'
import { PhicSetting } from '../../domain/phicSetting'
import { FacilitySettings } from '../../domain/settings'
import { IFacilityRepo } from '../../repo/facility.repo'
import { CreateFacilityDTO } from './createFacility.dto'

export class CreateFacilityUseCase
  implements UseCase<CreateFacilityDTO, Promise<Facility>> {
  private facilityRepo: IFacilityRepo

  constructor(fRepo: IFacilityRepo) {
    this.facilityRepo = fRepo
  }

  async execute(dto: CreateFacilityDTO): Promise<Facility> {
    const facilityCode = FacilityCode.create({
      value: dto.code,
      name: dto.name
    })

    const phicsettings = PhicSetting.create(dto.settings.phic)
    const address = Address.create(dto.address)
    const settings = FacilitySettings.create({
      phic: phicsettings.getValue()
    })

    const facility = Facility.create({
      address: address.getValue(),
      settings: settings.getValue(),
      name: dto.name,
      code: facilityCode.getValue()
    })

    await this.facilityRepo.save(facility.getValue())

    return facility.getValue()
  }
}
