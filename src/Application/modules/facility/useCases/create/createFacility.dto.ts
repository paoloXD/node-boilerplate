import { AddressDTO } from '@/Application/modules/shared/address/dto/address.dto'

export interface PhicSetting {
  accreditationNumber: string
  key: string
  accreditationName: string
}

export interface FacilitySettingsDTO {
  phic: PhicSetting
}

export interface CreateFacilityDTO {
  name: string
  code?: string
  address: AddressDTO
  settings: FacilitySettingsDTO
}
