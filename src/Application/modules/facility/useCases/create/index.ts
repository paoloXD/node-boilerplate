import { FacilityRepo } from '../../repo/facility.repo'
import { CreateFacilityController } from './createFacility.controller'
import { CreateFacilityUseCase } from './createFacility.useCase'

const facilityRepo = new FacilityRepo()
const createFacilityUseCase = new CreateFacilityUseCase(facilityRepo)
const createFacilityController = new CreateFacilityController(
  createFacilityUseCase
)

export { createFacilityController }
