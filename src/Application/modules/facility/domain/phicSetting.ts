import { Result } from '@/Core/domain/Result'
import { ValueObject } from '@/Core/domain/ValueObject'

export interface PhicSettingsProp {
  accreditationNumber: string
  key: string
  accreditationName: string
}

export class PhicSetting extends ValueObject<PhicSettingsProp> {
  private constructor(props: PhicSettingsProp) {
    super(props)
  }

  public static create(props: PhicSettingsProp): Result<PhicSetting> {
    return Result.ok<PhicSetting>(
      new PhicSetting({
        accreditationName: props.accreditationName,
        accreditationNumber: props.accreditationNumber,
        key: props.key
      })
    )
  }

  get accreditationNumber(): string {
    return this.props.accreditationNumber
  }

  get key(): string {
    return this.props.key
  }

  get accreditationName(): string {
    return this.props.accreditationName
  }
}
