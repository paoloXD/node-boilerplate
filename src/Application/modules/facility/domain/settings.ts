import { Result } from '@/Core/domain/Result'
import { ValueObject } from '@/Core/domain/ValueObject'
import { PhicSetting } from './phicSetting'

export interface facilitySettingsProps {
  phic: PhicSetting
}

export class FacilitySettings extends ValueObject<facilitySettingsProps> {
  private constructor(props: facilitySettingsProps) {
    super(props)
  }
  public static create(props: facilitySettingsProps): Result<FacilitySettings> {
    const phicOrfail = PhicSetting.create(props.phic)

    return Result.ok<FacilitySettings>(
      new FacilitySettings({
        phic: phicOrfail.getValue()
      })
    )
  }

  get phic(): PhicSetting {
    return this.props.phic
  }
}
