import { AggregateRoot } from '@/Core/domain/AggregateRoot'
import { Result } from '@/Core/domain/Result'
import { UniqueEntityID } from '@/Core/domain/UniqueEntity'
import { Address } from '../../shared/address/domain/address'
import { FacilityCode } from './facilityCode'
import { FacilitySettings } from './settings'

export interface FacilityProps {
  name: string
  code: FacilityCode
  address: Address
  settings: FacilitySettings
}

export class Facility extends AggregateRoot<FacilityProps> {
  private constructor(prop: FacilityProps, id?: UniqueEntityID) {
    super(prop, id)
  }

  public static create(
    prop: FacilityProps,
    id?: UniqueEntityID
  ): Result<Facility> {
    return Result.ok<Facility>(new Facility(prop, id))
  }

  get name(): string {
    return this.props.name
  }

  get code(): FacilityCode {
    return this.props.code
  }

  get address(): Address {
    return this.props.address
  }

  get settings(): FacilitySettings {
    return this.props.settings
  }
}
