import { Result } from '@/Core/domain/Result'
import { ValueObject } from '@/Core/domain/ValueObject'

export interface FacilityCodeProps {
  name: string
  value?: string
}

export class FacilityCode extends ValueObject<FacilityCodeProps> {
  private static REGEX = /\b(\w)/g

  get value(): string {
    return this.props.value as string
  }

  private constructor(props: FacilityCodeProps) {
    super(props)
  }

  public static create(props: FacilityCodeProps): Result<FacilityCode> {
    let code = props.value

    if (!props.value) {
      const matches = props.name.match(this.REGEX)
      code = matches?.join('')
    }

    return Result.ok<FacilityCode>(
      new FacilityCode({
        name: props.name,
        value: code
      })
    )
  }
}
