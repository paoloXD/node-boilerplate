import { FacilityModel } from '@/Application/models'
import { Mapper } from '@/Core/infra/Mapper'
import { AddressMapper } from '../../shared/address/mapper/address.map'
import { Facility } from '../domain/facility'
import { FacilityDTO } from '../dtos/facility.dto'
import { FacilitySettingMapper } from './settings.map'

export class FacilityMapper implements Mapper {
  public static toDto(f: Facility): FacilityDTO {
    const id = f.id.toValue() as string
    return {
      id,
      name: f.name,
      code: f.code.value,
      address: AddressMapper.toDto(f.address),
      settings: FacilitySettingMapper.toDto(f.settings)
    }
  }

  public static toPersist(f: Facility): FacilityModel {
    return this.toDto(f)
  }
}
