import { Mapper } from '@/Core/infra/Mapper'
import { FacilitySettings } from '../domain/settings'
import { FacilitySettingsDTO } from '../useCases/create/createFacility.dto'

export class FacilitySettingMapper implements Mapper {
  public static toDto(setting: FacilitySettings): FacilitySettingsDTO {
    return {
      phic: {
        accreditationName: setting.phic.accreditationName,
        accreditationNumber: setting.phic.accreditationNumber,
        key: setting.phic.key
      }
    }
  }
}
