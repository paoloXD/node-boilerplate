import { AddressDTO } from '../../shared/address/dto/address.dto'
import { FacilitySettingsDTO } from '../useCases/create/createFacility.dto'

export interface FacilityDTO {
  id: string
  name: string
  code: string
  address: AddressDTO
  settings: FacilitySettingsDTO
}
