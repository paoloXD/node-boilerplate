import { IRepo } from '@/Core/infra/Repo'
import { FirebaseRepo } from '@/Infra/database/firebase/FirebaseRepo'
import { Facility } from '../domain/facility'
import { FacilityMapper } from '../mappers/facility.map'

export interface IFacilityRepo extends IRepo<Facility> {
  getById(facilityId: string): Promise<Facility>
}

export class FacilityRepo extends FirebaseRepo implements IFacilityRepo {
  constructor() {
    super('facility')
  }

  getById(facilityId: string): Promise<Facility> {
    throw new Error('Method not implemented.')
  }

  async exists(id: string): Promise<boolean> {
    const ref = this.collection.doc(id)
    return await (await ref.get()).exists
  }

  async save(facility: Facility): Promise<Facility> {
    const data = FacilityMapper.toPersist(facility)

    const exist = await this.exists('33944002-4638-4296-8531-bda5f25fed35')

    console.log(exist)

    const ref = this.collection.doc(facility.id.toValue() as string)
    return ref
      .set(data)
      .then(() => {
        return facility
      })
      .catch((e) => {
        throw new Error(e)
      })
  }
}
