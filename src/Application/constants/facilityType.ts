export enum FacilityType {
  HOSPITAL = 'HOSPITAL',
  CLINIC = 'CLINIC'
}
