import { Router } from 'express'
import { facilityRoute } from '@/Application/modules/facility/infra/routes'

const routeV1 = Router()

routeV1.use('/facility', facilityRoute)

export { routeV1 }
