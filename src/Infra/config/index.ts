import dotenv from 'dotenv'
import { propertyValue } from '../helpers'
import firebase from './firebase.config'

dotenv.config()

const appConfig = {
  PORT: propertyValue(process.env, 'APP_PORT', 3000)
}

const firebaseConfig = firebase()

export { appConfig, firebaseConfig }
