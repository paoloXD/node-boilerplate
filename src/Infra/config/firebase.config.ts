import * as serviceAccount from './serviceAccount.json'

type FirebaseServiceAccount = {
  project_id: string
  [k: string]: string
}

export default (): FirebaseServiceAccount => {
  return serviceAccount
}
