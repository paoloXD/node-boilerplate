import express from 'express'
import bodyParser from 'body-parser'
import cors from 'cors'
import helmet from 'helmet'
import { routeV1 } from '../router/v1'
import { appConfig } from '../config'

const app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(cors())
app.use(helmet())

app.use('/api/v1', routeV1)

app.listen(appConfig.PORT, () => {
  console.log(`[App]: Server listening on ${appConfig.PORT}`)
})

export { app }
