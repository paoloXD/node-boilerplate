import { IRepo } from '@/Core/infra/Repo'
import { FireStore } from '.'

export abstract class FirebaseRepo implements IRepo<any> {
  protected collection: FirebaseFirestore.CollectionReference<
    FirebaseFirestore.DocumentData
  >

  constructor(collection: string) {
    this.collection = FireStore.collection(collection)
  }

  abstract save(t: any): Promise<any>

  exists(t: any): Promise<boolean> {
    throw new Error('Method not implemented.')
  }
}
