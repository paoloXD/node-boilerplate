import * as admin from 'firebase-admin'
import { firebaseConfig } from '@/Infra/config'

admin.initializeApp({
  credential: admin.credential.cert(firebaseConfig as admin.ServiceAccount),
  databaseURL: `https://${firebaseConfig.project_id}.firebaseio.com`
})

const FireStore = admin.firestore()

export { FireStore }
