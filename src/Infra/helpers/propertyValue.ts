export const propertyValue = <T>(
  obj: any,
  key: string,
  defaultValue: any = null
): T => {
  const path = key.split('.')
  try {
    for (let i = 0; i < path.length; i++) {
      if (defaultValue !== null && i + 1 === path.length) {
        obj[path[i]] = obj[path[i]] || defaultValue
      }

      obj = obj[path[i]]
    }

    switch (Object.prototype.toString.call(obj)) {
      case '[object Undefined]':
        return defaultValue
      case '[object Boolean]':
        return obj
      case '[object Array]':
        return obj.length ? obj : defaultValue
      default:
        return obj
    }
  } catch (e) {
    return defaultValue
  }
}
