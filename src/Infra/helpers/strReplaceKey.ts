export const strReplaceKey = (
  replacePairs: { [key: string]: string },
  str: string
): string => {
  let key: string
  let rex: RegExp

  for (key in replacePairs) {
    if (Object.prototype.hasOwnProperty.call(replacePairs, key)) {
      rex = new RegExp(key, 'g')
      str = str.replace(rex, replacePairs[key])
    }
  }

  return str
}
