import { Request, Response, NextFunction } from 'express'

export interface RequestValidator {
  validate(req: Request, res: Response, next: NextFunction): void
}
