export interface IRepo<AcceptT> {
  exists(t: any): Promise<boolean>
  save(t: AcceptT): Promise<any>
}
