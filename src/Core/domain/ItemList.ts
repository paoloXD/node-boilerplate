export abstract class ItemList<T> {
  public currentItems: T[]

  constructor(items?: T[]) {
    this.currentItems = items ? items : []
  }
}
