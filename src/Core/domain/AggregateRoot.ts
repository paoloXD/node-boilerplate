import { Entity } from './Entity'
import { UniqueEntityID } from './UniqueEntity'

export abstract class AggregateRoot<T> extends Entity<T> {
  // private _domainEvents: IDomainEvent[] = []

  get id(): UniqueEntityID {
    return this._id
  }
}
