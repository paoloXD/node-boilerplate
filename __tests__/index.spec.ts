import {
  FacilityProps,
  Facility
} from '../src/Application/modules/facility/domain/facility'
import { FacilityCode } from '../src/Application/modules/facility/domain/facilityCode'
import { FacilitySettings } from '../src/Application/modules/facility/domain/settings'
import { Address } from '../src/Application/modules/shared/address/domain/address'
import { Result } from '../src/Core/domain/Result'

describe('Facility Domain', () => {
  const requestData = {
    name: 'THE MEDICAL CITY',
    address: {
      brgy: '',
      lat: 0,
      long: 0,
      province: 'string',
      region: '',
      zip: ''
    }
  }

  it('should create id if no given', () => {
    const codeOrfail: Result<FacilityCode> = FacilityCode.create({
      value: '',
      name: requestData.name
    })
    const addressOrfail: Result<Address> = Address.create(requestData.address)

    if (codeOrfail.isFailure) {
      console.log(codeOrfail.errorValue())
    }

    if (addressOrfail.isFailure) {
      throw new Error('address fail')
    }

    const createProps: FacilityProps = {
      name: requestData.name,
      code: codeOrfail.getValue(),
      address: addressOrfail.getValue(),
      settings: {}
    }

    const facilityOrFail: Result<Facility> = Facility.create(createProps)

    if (facilityOrFail.isFailure) {
      throw new Error('user fail')
    }

    const facility: Facility = facilityOrFail.getValue()

    expect(facility.id).toBeTruthy()
  })

  it('should not create id if given', () => {
    const uuid = 'asd-asd-asd-asd'

    const codeOrfail: Result<FacilityCode> = FacilityCode.create({
      value: '',
      name: requestData.name
    })
    const addressOrfail: Result<Address> = Address.create(requestData.address)

    if (codeOrfail.isFailure) {
      console.log(codeOrfail.errorValue())
    }

    if (addressOrfail.isFailure) {
      throw new Error('address fail')
    }

    const createProps: FacilityProps = {
      name: requestData.name,
      code: codeOrfail.getValue(),
      address: addressOrfail.getValue(),
      settings: {
        phic: {
          accreditationNumber: 'test',
          key: 'test',
          accreditationName: 'test'
        }
      }
    }

    const settingsOrFail = FacilitySettings.create(createProps.settings)

    const facilityOrFail: Result<Facility> = Facility.create(createProps, uuid)

    if (facilityOrFail.isFailure) {
      throw new Error('user fail')
    }

    const facility: Facility = facilityOrFail.getValue()

    console.log(facility.code.value)

    expect(facility.id).toEqual(uuid)
  })
})
